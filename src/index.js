import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider, useDispatch } from 'react-redux';

import { ToastContainer } from 'react-toastify';
import NavBar from './components/NavBar/NavBarContainer';

import store from './store/createStore';
import { appOperations } from './modules/app';

import Footer from './components/Footer/Footer';
import Routes from './routes/routes';

import './styles/App.css';
import './styles/buttons.css';
import './styles/mobile.css';
import './styles/tablet.css';
import 'react-toastify/dist/ReactToastify.css';

const App = () => {
  const dispatch = useDispatch();
  dispatch(appOperations.init());

  return (
    <BrowserRouter>
      <NavBar />
      <ToastContainer />
      <Routes />
      <Footer />
    </BrowserRouter>
  );
};

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.querySelector('#root')
);
