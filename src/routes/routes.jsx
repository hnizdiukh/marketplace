import React from 'react';
import { Switch, Route } from 'react-router-dom';

import LoginForm from 'src/components/LoginForm/LoginFormContainer';
import RegisterForm from 'src/components/RegisterForm/RegisterFormContainer';
import Homepage from 'src/components/Homepage/HomepageContainer';
import EditProfilePage from 'src/components/Profile/EditProfile/EditProfileContainer';
import ProfilePage from 'src/components/Profile/ProfilePage/ProfileContainer';
import Wishlist from 'src/components/WishlistPage/Wishlist';
import SellProductPage from 'src/components/SellProductPage/SellProductContainer';
import ProductPage from 'src/components/Product/ProductPage/ProductPage';
import EditProduct from 'src/components/Product/EditProduct/EditProductContainer';
import ChatModalContainer from 'src/components/Chat/ChatModal/ChatModalContainer';
import ChatPage from 'src/components/Chat/ChatPage';
import NotFound from 'src/components/NotFound/NotFoundPage';
import routes from './config';

const Routes = () => (
  <>
    <Switch>
      <Route path={routes.HOME} exact component={Homepage} />
      <Route path={routes.LOGIN} component={LoginForm} />
      <Route path={routes.REGISTER} component={RegisterForm} />
      <Route path={routes.USER_ID} component={ProfilePage} />
      <Route path={routes.PROFILE} component={ProfilePage} />
      <Route path={routes.EDIT_PROFILE} component={EditProfilePage} />
      <Route path={routes.WISH_LIST} component={Wishlist} />
      <Route path={routes.SELL} component={SellProductPage} />
      <Route path={routes.EDIT_PRODUCT_ID} component={EditProduct} />
      <Route path={routes.PRODUCT_ID} component={ProductPage} />
      <Route path={routes.CHAT_ID} component={ChatPage} />
      <Route path={routes.CHAT} component={ChatPage} />
      <Route component={NotFound} />
    </Switch>
    <Route path="/" component={ChatModalContainer} />
  </>
);

export default Routes;
