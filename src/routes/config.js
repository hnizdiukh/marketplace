const routes = {
  HOME: '/',
  LOGIN: '/login',
  REGISTER: '/register',
  FORGOT: '/forgot',
  SELL: '/sell',
  WISH_LIST: '/wish_list',
  PROFILE: '/profile',
  USER: '/user',
  USER_ID: '/user/:userId',
  EDIT_PROFILE: '/edit_profile',
  PRODUCTS: '/products',
  PRODUCT: '/product',
  EDIT_PRODUCT_ID: '/product/:productId/edit',
  PRODUCT_ID: '/product/:productId',
  CHAT_MODAL: '/chat',
  CHAT: '/chat',
  CHAT_ID: '/chat/:chatId',

  PRIVACY: '/privacy',
};

export default routes;
