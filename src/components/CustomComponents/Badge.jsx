import React from 'react';
import generateAvatar from 'src/utils/avatarGenerator';

const Badge = ({ user, onClick, wrapperClassName, size, fontSize }) => {
  const { fullName, _id, avatar } = user;
  let generatedAvatar;
  if (!avatar) {
    generatedAvatar = generateAvatar(fullName, _id, 60, 80);
  }

  return (
    <span className={wrapperClassName}>
      {avatar ? (
        <img
          className="badge"
          onClick={onClick}
          onKeyPress={onClick}
          role="presentation"
          src={avatar}
          alt="Avatar"
          style={{ width: size, height: size, objectFit: 'cover' }}
        />
      ) : (
        <div
          onClick={onClick}
          onKeyPress={onClick}
          role="button"
          tabIndex={0}
          className="badge"
          style={{
            backgroundColor: generatedAvatar.color,
            height: size,
            width: size,
            fontSize,
          }}
        >
          {generatedAvatar.badge}
        </div>
      )}
    </span>
  );
};

export default Badge;
