import React, { useState } from 'react';
import { productImagePlaceholder } from 'src/configs/variables';
import getProductImage from 'src/utils/getProductImage';
import Item from './SliderItem';
import Icon from '../Icon/Icon';

import './slider.css';

const ImageSlider = ({ photos, productName }) => {
  const [currentImg, setCurrentImg] = useState(0);

  if (!photos || !photos.length) {
    return <img src={productImagePlaceholder} alt={productName} />;
  } else if (photos.length === 1) {
    return <img src={getProductImage(photos)} alt={productName} />;
  }

  return (
    <div className="slider">
      <span
        onClick={() => setCurrentImg(Math.abs((currentImg - 1) % photos.length))}
        onKeyPress={() => setCurrentImg(Math.abs((currentImg - 1) % photos.length))}
        className="slider-btn prev-btn"
        role="button"
        tabIndex={0}
      >
        <Icon name="next" width="30" height="35" />
      </span>

      {photos.map((photo, index) => (
        <Item
          key={photo + productName}
          index={index}
          src={photo}
          current={currentImg}
          productName={productName}
        />
      ))}

      <span
        onClick={() => setCurrentImg((currentImg + 1) % photos.length)}
        onKeyPress={() => setCurrentImg((currentImg + 1) % photos.length)}
        className="slider-btn next-btn"
        role="button"
        tabIndex={0}
      >
        <Icon name="next" width="30" height="35" />
      </span>
    </div>
  );
};

export default ImageSlider;
