import React from 'react';
import { createPortal } from 'react-dom';

const Modal = (props) =>
  createPortal(
    <div
      className="modal"
      onClick={props.onClick}
      onKeyPress={props.onClick}
      role="button"
      tabIndex={0}
    >
      {props.children}
    </div>,
    document.getElementById('modal_root')
  );

export default Modal;
