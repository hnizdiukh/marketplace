import React from 'react';
import routes from 'src/routes/config';
import { Link } from 'react-router-dom';

const NotFound = () => (
  <>
    <h1 className="not-found" style={{ fontSize: '100px', lineHeight: '115px' }}>
      404 Not Found
    </h1>
    <Link to={routes.HOME} className="homepage-link">
      Go to homepage
    </Link>
  </>
);

export default NotFound;
