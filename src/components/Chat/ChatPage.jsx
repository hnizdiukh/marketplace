import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Api from 'src/api';
import { chatOperations } from 'src/modules/chat';
import { toast } from 'react-toastify';
import { Redirect } from 'react-router-dom';
import routes from 'src/routes/config';
import { messagesOperations } from 'src/modules/messages';
import SimpleBar from 'simplebar-react';
import useWindowSize from 'src/utils/useWindowSize';
import Messages from './MessagesBlock/MessagesContainer';
import ChatItem from './ChatItem';
import UnloginedRedirect from '../CustomComponents/Unlogined';

import 'simplebar/dist/simplebar.min.css';
import Loading from '../CustomComponents/Loading/Loading';

const ChatPage = (props) => {
  const { match } = props;
  const dispatch = useDispatch();
  const { isLoggedIn } = Api.Auth;
  const windowSize = useWindowSize();
  const chats = useSelector((state) => state.chat.chats);
  const isChatsLoading = useSelector((state) => state.chat.fetchChats.isLoading);
  const user = useSelector((state) => state.viewer.user);
  const [isChatListShown, setChatListShown] = useState(false);

  useEffect(() => {
    dispatch(chatOperations.fetchChats());
  }, [dispatch]);

  useEffect(() => {
    if (match.params.chatId) {
      dispatch(messagesOperations.fetchMessages(match.params.chatId));
      setChatListShown(false);
    } else {
      setChatListShown(true);
    }
  }, [dispatch, match.params.chatId]);

  if (
    match.params.chatId &&
    chats.length &&
    !chats.filter((chat) => chat._id === match.params.chatId).length
  ) {
    toast.error('No such chat found!');
    return <Redirect to={routes.CHAT} />;
  }

  if (!isLoggedIn) {
    return <UnloginedRedirect />;
  }

  if (isChatsLoading || !user) {
    return <Loading />;
  }

  if (!chats.length) {
    return <div className="not-found">You don&apos;t have any chats yet</div>;
  }

  const chatSelected = chats.find((c) => c._id === match.params.chatId);

  if (!match.params.chatId) {
    return <Redirect to={`${routes.CHAT}/${chats[0]._id}`} />;
  }

  return (
    <div className="chat-page" style={{ height: windowSize.height - 78 }}>
      <div className="chat-list-wrap" style={{ display: isChatListShown ? 'block' : '' }}>
        <div className="chat-list">
          <SimpleBar style={{ maxHeight: windowSize.height - 78 }}>
            {chats.length
              ? chats.map((chat) => (
                  <ChatItem
                    chat={chat}
                    key={chat._id}
                    userId={user._id}
                    current={props.match.params.chatId}
                    setChatListShown={setChatListShown}
                  />
                ))
              : null}
          </SimpleBar>
        </div>
      </div>
      <Messages
        chat={chatSelected}
        setChatListShown={setChatListShown}
        isChatListShown={isChatListShown}
      />
    </div>
  );
};

export default ChatPage;
