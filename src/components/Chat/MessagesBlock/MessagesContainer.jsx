import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import getProductImage from 'src/utils/getProductImage';
import useWindowSize from 'src/utils/useWindowSize';
import MessagesHeader from './MessagesHeader';
import MessagesBody from './MessagesBody';
import MessagesFooter from './MessagesFooter';

import 'emoji-mart/css/emoji-mart.css';

const Messages = ({ chat, setChatListShown, isChatListShown }) => {
  const [messageInput, setMessageInput] = useState('');
  const windowSize = useWindowSize();
  const messages = useSelector((state) => state.messages.messages);
  const isMessagesLoading = useSelector((state) => state.messages.fetchMessages.isLoading);
  const isChatsLoading = useSelector((state) => state.chat.fetchChats.isLoading);
  const user = useSelector((state) => state.viewer.user);

  const [displayEmojiPicker, setDisplayEmojiPicker] = useState(false);

  const onEmojiClick = (emojiObject) => {
    setMessageInput(messageInput + emojiObject.native);
  };

  if (!chat && !isMessagesLoading && !isChatsLoading) {
    return <div className="no-chat-selected not-found">Please select chat to start messaging</div>;
  }

  const imageSrc = getProductImage(chat.product.photos);

  return (
    <>
      <div className="messages-wrap" style={{ display: isChatListShown ? 'none' : '' }}>
        <MessagesHeader
          setChatListShown={setChatListShown}
          chat={chat}
          imageSrc={imageSrc}
          userId={user._id}
        />

        <MessagesBody
          messages={messages}
          windowSize={windowSize}
          chat={chat}
          messageInput={messageInput}
          isMessagesLoading={isMessagesLoading}
          userId={user._id}
        />

        <MessagesFooter
          chat={chat}
          messageInput={messageInput}
          setMessageInput={setMessageInput}
          setDisplayEmojiPicker={setDisplayEmojiPicker}
          displayEmojiPicker={displayEmojiPicker}
          onEmojiClick={onEmojiClick}
        />
      </div>
    </>
  );
};

export default Messages;
