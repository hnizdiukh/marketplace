import React, { useRef } from 'react';
import { Picker } from 'emoji-mart';
import Icon from 'src/components/CustomComponents/Icon/Icon';
import { useDispatch } from 'react-redux';
import { messagesOperations } from 'src/modules/messages';

const MessagesFooter = ({
  messageInput,
  setMessageInput,
  setDisplayEmojiPicker,
  displayEmojiPicker,
  onEmojiClick,
  chat,
}) => {
  const dispatch = useDispatch();

  const handleMessageSubmit = (e) => {
    e.preventDefault();
    if (!messageInput) return;
    dispatch(messagesOperations.sendMessage({ text: messageInput }, chat._id));
    setMessageInput('');
  };

  const emojiRef = useRef();

  return (
    <div className="messages-footer">
      <form onSubmit={handleMessageSubmit} className="submit-message-form">
        <input
          className="message-input"
          type="text"
          value={messageInput}
          placeholder="Type your message here..."
          onChange={(e) => setMessageInput(e.target.value)}
          onKeyPress={(e) => e.key === 'Enter' && handleMessageSubmit(e)}
        />
        <span
          className="emoji-picker-btn"
          onClick={() => setDisplayEmojiPicker(!displayEmojiPicker)}
          onKeyPress={() => setDisplayEmojiPicker(!displayEmojiPicker)}
          role="button"
          tabIndex={0}
        >
          <Icon name="emoji" width="22" height="22" />
        </span>

        <span
          className="emoji-picker"
          style={{ display: displayEmojiPicker ? 'flex' : 'none' }}
          ref={emojiRef}
        >
          <Picker onSelect={onEmojiClick} />
        </span>
        <button type="submit" name="button" onClick={() => setDisplayEmojiPicker(false)}>
          Send
        </button>
      </form>
    </div>
  );
};

export default MessagesFooter;
