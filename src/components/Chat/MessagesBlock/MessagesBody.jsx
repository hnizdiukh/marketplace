import React, { useEffect } from 'react';
import Moment from 'react-moment';
import SimpleBar from 'simplebar-react';
import Loading from 'src/components/CustomComponents/Loading/Loading';

const MessagesBody = ({ messages, windowSize, chat, messageInput, isMessagesLoading, userId }) => {
  const messagesEndRef = React.createRef(null);

  useEffect(() => {
    if (messagesEndRef.current && !messageInput) {
      messagesEndRef.current.scrollIntoView();
    }
  }, [messagesEndRef, messageInput]);

  return (
    <div className="messages-body">
      {isMessagesLoading ? (
        <Loading />
      ) : (
        <SimpleBar style={{ maxHeight: windowSize.height - 212 }} autoHide={false}>
          {messages.map((message) => (
            <div
              className={userId !== message.ownerId ? 'message-recieved' : 'message-send'}
              key={`${Math.random()}${chat._id}`}
            >
              <div className="message-text">{message.text}</div>
              <div className="message-time-under">
                <Moment fromNow>{message.createdAt}</Moment>
              </div>
            </div>
          ))}
          <span ref={messagesEndRef} />
        </SimpleBar>
      )}
    </div>
  );
};

export default MessagesBody;
