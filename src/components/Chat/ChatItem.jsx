import React from 'react';
import Moment from 'react-moment';
import { Link } from 'react-router-dom';
import routes from 'src/routes/config';
import { useSelector } from 'react-redux';
import getProductImage from 'src/utils/getProductImage';

const ChatItem = ({ chat, current, setChatListShown, userId }) => {
  const calendarStrings = {
    lastDay: 'ddd',
    sameDay: 'HH:mm',
    lastWeek: 'DD.MM',
    sameElse: 'DD.MM',
  };

  const imageSrc = getProductImage(chat.product.photos);

  const lastMessageState = useSelector((state) => state.messages.lastMessage);

  const chatWith = chat.participants.find((u) => u._id !== userId);

  const lastMessage =
    lastMessageState && lastMessageState.chatId === chat._id
      ? lastMessageState.text
      : chat.lastMessage
      ? chat.lastMessage.text
      : '';

  return (
    <Link
      to={`${routes.CHAT}/${chat._id}`}
      className={chat._id === current ? 'current-chat chat-item' : 'chat-item'}
      onClick={() => setChatListShown(false)}
    >
      <div className="chat-with">
        <span className="chat-with-fullName">{chatWith ? chatWith.fullName : ''}</span>
        <span className="chat-last-message">{lastMessage}</span>
      </div>
      <div className="product-with">
        <span className="product-image">
          <img src={imageSrc} alt={chat.product.title} />
        </span>
        <div className="chat-product-details">
          <span className="chat-product-name">{chat.product.title}</span>
          <span className="chat-product-price">${chat.product.price.toFixed(2)}</span>
        </div>
      </div>
      <div className="message-time">
        {chat.lastMessage ? (
          <Moment calendar={calendarStrings}>{chat.lastMessage.createdAt}</Moment>
        ) : (
          <Moment calendar={calendarStrings}>{chat.createdAt}</Moment>
        )}
      </div>
    </Link>
  );
};

export default ChatItem;
