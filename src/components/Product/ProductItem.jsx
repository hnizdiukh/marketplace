import React from 'react';
import { Link } from 'react-router-dom';
import routes from 'src/routes/config';
import useAddToWishlist from 'src/utils/useAddToWishlist';
import { useSelector } from 'react-redux';
import getProductImage from 'src/utils/getProductImage';
import Icon from '../CustomComponents/Icon/Icon';

const Product = ({ product }) => {
  const user = useSelector((state) => state.viewer.user);
  const [isSaved, setSaved] = useAddToWishlist(user, product);

  const handleAddToWishlish = () => {
    setSaved(isSaved);
  };

  const isUserOwner = user ? product.ownerId === user._id : false;

  return (
    <div className="product-thumbnail">
      <div className="product-img">
        <Link to={`${routes.PRODUCT}/${product._id}`}>
          <img src={getProductImage(product.photos)} alt={product.title} />
        </Link>
      </div>
      {isUserOwner ? (
        ''
      ) : (
        <div className="add-to-wishlist-container">
          <span
            className="add-to-wishlist-wrap"
            onClick={handleAddToWishlish}
            onKeyPress={handleAddToWishlish}
            role="button"
            tabIndex={0}
          >
            {isSaved ? (
              <Icon name="heart_filled" width="16" height="17" fill="#349a89" />
            ) : (
              <Icon name="heart" width="16" height="17" />
            )}
          </span>
        </div>
      )}
      <div>
        <Link to={`${routes.PRODUCT}/${product._id}`}>
          <p>{product.title}</p>
        </Link>
        <b>${product.price.toFixed(2)}</b>
      </div>
    </div>
  );
};
export default Product;
