import React from 'react';
import ProductForm from 'src/components/CustomComponents/Forms/ProductForm';

const EditProductPage = ({ onSubmit, schema, initialValues, handleImageUpload }) => (
  <main className="container">
    <div className="sell-container">
      <div className="sell-wrap">
        <h1>Edit product</h1>
        <ProductForm
          onSubmit={onSubmit}
          schema={schema}
          initialValues={initialValues}
          handleImageUpload={handleImageUpload}
        />
      </div>
    </div>
  </main>
);

export default EditProductPage;
