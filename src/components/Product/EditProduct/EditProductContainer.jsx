import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ProductSchema } from 'src/configs/schemas';
import Loading from 'src/components/CustomComponents/Loading/Loading';
import { productsOperations } from 'src/modules/products';
import Api from 'src/api';
import routes from 'src/routes/config';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import EditProductPage from './EditProductPage';

const EditProduct = ({ match }) => {
  const product = useSelector((state) => state.products.product);
  const isUpdating = useSelector((state) => state.products.updateProduct.isLoading);
  const [images, setImages] = useState([]);
  const dispatch = useDispatch();
  const history = useHistory();

  const { productId } = match.params;

  if (!product) {
    dispatch(productsOperations.getProduct(productId));
    return <Loading />;
  }

  const fetchImage = async (image) => {
    if (typeof image === 'string') {
      return image;
    }
    const res = await Api.Image.upload(image);
    return res.data.data.link;
  };

  const sendImages = async (imgs) => Promise.all(imgs.map((image) => fetchImage(image)));

  const handleAddProduct = async (values) => {
    try {
      let imagesResponsesArray;
      const imageLinksArray = [];

      if (images) {
        imagesResponsesArray = await sendImages(images);
      }

      if (imagesResponsesArray && Array.isArray(imagesResponsesArray)) {
        imagesResponsesArray.map((imageRes) => imageLinksArray.push(imageRes));
      }

      imageLinksArray.forEach((link) => {
        if (typeof link !== 'string' || !link.length) {
          throw new Error('Images are not uploaded');
        }
      });

      const updatedProduct = { ...values, photos: imageLinksArray };

      await dispatch(productsOperations.updateProduct(updatedProduct));
      toast.success('Product updated');
      history.push(routes.HOME);
    } catch (error) {
      toast.error(error.message || 'Can not upload this product');
    }
  };

  const handleImageUpload = (imgs) => {
    setImages(imgs);
  };

  const initialValues = {
    ...product,
  };

  if (isUpdating) {
    return <Loading />;
  }

  return (
    <EditProductPage
      onSubmit={handleAddProduct}
      schema={ProductSchema}
      initialValues={initialValues}
      product={product}
      handleImageUpload={handleImageUpload}
    />
  );
};

export default EditProduct;
