import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useStore, useSelector } from 'react-redux';
import { productsOperations } from 'src/modules/products';
import { toast } from 'react-toastify';
import routes from 'src/routes/config';
import Loading from '../../CustomComponents/Loading/Loading';
import ProductDetails from './ProductDetailsBlock';
import AuthorDetails from './AuthorDetailsBlock';

import '../productpage.css';

const ProductPage = (props) => {
  const store = useStore();
  const { productId } = useParams();
  const dispatch = useDispatch();
  const [product, setProduct] = useState(null);
  const user = useSelector((state) => state.viewer.user);

  useEffect(() => {
    const fetchProduct = async () => {
      await dispatch(productsOperations.getProduct(productId));
      await dispatch(productsOperations.savedProducts());
      const productState = await store.getState();
      if (productState.products.getProduct.isError) {
        toast.error('No such product');
        props.history.push(routes.HOME);
      } else {
        setProduct(productState.products.product);
      }
    };
    fetchProduct();
  }, [store, dispatch, productId, props.history]);

  const handleRemove = async () => {
    try {
      await dispatch(productsOperations.deleteProduct(productId));
      props.history.push(routes.HOME);
      toast.success('Product is removed');
    } catch (error) {
      toast.error('Can not remove product');
    }
  };

  const renderElement = !product ? (
    <Loading />
  ) : (
    <main className="container">
      <div className="product-page">
        <ProductDetails product={product} />
        <AuthorDetails
          product={product}
          user={user}
          matchUrl={props.match.url}
          handleRemove={handleRemove}
        />
      </div>
    </main>
  );

  return renderElement;
};

export default ProductPage;
