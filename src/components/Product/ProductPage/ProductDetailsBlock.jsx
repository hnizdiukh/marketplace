/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import Moment from 'react-moment';
import { Link } from 'react-router-dom';
import ImageSlider from '../../CustomComponents/Slider/ImageSlider';
import Icon from '../../CustomComponents/Icon/Icon';

const ProductDetails = ({ product }) => {
  return (
    <div className="product-details">
      <ImageSlider photos={product.photos} productName={product.title} />
      <div className="pdp-content-container">
        <p id="price-pdp">
          <b>${product.price.toFixed(2)}</b>
        </p>
        <p id="name-pdp">
          {product.title}
          <span>
            <Moment fromNow>{product.createdAt}</Moment>
          </span>
        </p>
        <p className="location-pdp">
          <Icon name="location" height="17" width="11" />
          <Link to={`/?location=${product.location.replace(' ', '+')}`} className="location-pdp">
            {product.location || ''}
          </Link>
        </p>
        <p className="description-pdp">{product.description}</p>
      </div>
    </div>
  );
};

export default ProductDetails;
