import React from 'react';
import Loading from '../CustomComponents/Loading/Loading';
import ProductList from '../Product/ProductList';

const Homepage = ({ products, hanldeLoadMore, isLoadingMore, limit, search }) => (
  <main className="container">
    <div>
      <ProductList products={products} />

      {products.length % limit === 0 && !search ? (
        <div className="center col">
          {isLoadingMore && <Loading height="auto" />}
          <button className="load-more-btn" onClick={hanldeLoadMore} type="button">
            Load more
          </button>
        </div>
      ) : (
        null
      )}
    </div>
  </main>
);

export default Homepage;
