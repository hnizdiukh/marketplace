import React, { useEffect } from 'react';
import Loading from 'src/components/CustomComponents/Loading/Loading';
import Api from 'src/api';
import { useSelector, useDispatch } from 'react-redux';
import { productsOperations } from 'src/modules/products';
import { userOperations } from 'src/modules/user';
import NotFound from 'src/components/NotFound/NotFoundPage';
import UnloginedRedirect from 'src/components/CustomComponents/Unlogined';
import Profile from './ProfilePage';

import '../profile.css';

const ProfileContainer = ({ match }) => {
  const userIdParam = match.params.userId;
  const { isLoggedIn } = Api.Auth;
  const currentUser = useSelector((state) => state.viewer.user);
  const userProducts = useSelector((state) => state.products.userProducts);
  const loadedUser = useSelector((state) => state.user.user);
  const isUserLoading = useSelector((state) => state.user.fetchUser.isLoading);
  const isUserLoadError = useSelector((state) => state.user.fetchUser.isError);
  const isProductsLoading = useSelector((state) => state.products.getUserProducts.isLoading);
  const dispatch = useDispatch();

  useEffect(() => {
    if (userIdParam) {
      dispatch(userOperations.fetchUser(userIdParam));
      dispatch(productsOperations.getUserProducts(userIdParam));
    } else if (currentUser) {
      dispatch(productsOperations.getUserProducts(currentUser._id));
    }
  }, [dispatch, currentUser, userIdParam, isLoggedIn]);

  const user = userIdParam ? loadedUser : currentUser;

  if (isUserLoadError) {
    return <NotFound />;
  }

  if (!isLoggedIn) {
    return <UnloginedRedirect />;
  }

  if (!user || isProductsLoading || isUserLoading) return <Loading />;

  return (
    <Profile
      user={user}
      userProducts={userProducts}
      matchUrl={match.url}
      isCurrentUser={!!userIdParam}
    />
  );
};

export default ProfileContainer;
