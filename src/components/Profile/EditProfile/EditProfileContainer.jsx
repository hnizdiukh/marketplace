import React from 'react';
import UnloginedRedirect from 'src/components/CustomComponents/Unlogined';
import Api from 'src/api';
import { ProfileSchema } from 'src/configs/schemas';
import { useSelector } from 'react-redux';
import EditProfilePage from './EditProfilePage';
import Loading from '../../CustomComponents/Loading/Loading';

import '../profile.css';

const EditProfileContainer = () => {
  const { isLoggedIn } = Api.Auth;
  const user = useSelector((state) => state.viewer.user);
  const isViewerLoading = useSelector((state) => state.viewer.fetchViewer.isLoading);
  const isAppLoading = useSelector((state) => state.app.isLoading);

  const initialValues = {
    phone: user?.phone || '',
    fullName: user?.fullName || '',
    location: user?.location || '',
  };

  if (!isLoggedIn) {
    return <UnloginedRedirect />;
  }

  const ProfileElement =
    isAppLoading || isViewerLoading || !user ? (
      <Loading />
    ) : (
      <EditProfilePage user={user} schema={ProfileSchema} initialValues={initialValues} />
    );

  return ProfileElement;
};

export default EditProfileContainer;
