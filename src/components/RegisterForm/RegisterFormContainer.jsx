import React from 'react';
import { useHistory } from 'react-router-dom';
import routes from 'src/routes/config';
import { useDispatch, useSelector } from 'react-redux';
import { authOperations } from 'src/modules/auth';
import store from 'src/store/createStore';
import { SignupSchema } from 'src/configs/schemas';
import { toast } from 'react-toastify';
import Loading from '../CustomComponents/Loading/Loading';
import RegisterForm from './RegisterFormComponent';

const RegisterFormContainer = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.auth.register.isLoading);

  const initialValues = { email: '', fullName: '', password: '', passwordConfirmation: '' };

  const onSubmit = async (values) => {
    await dispatch(
      authOperations.register({
        email: values.email,
        fullName: values.fullName,
        password: values.password,
      })
    );
    const storeState = await store.getState();
    const { isSuccess } = storeState.auth.register;
    if (isSuccess) {
      history.push(routes.HOME);
    } else {
      toast.error('User with this email is already exist');
    }
  };
  const Register = isLoading ? (
    <Loading />
  ) : (
    <RegisterForm schema={SignupSchema} initialValues={initialValues} onSubmit={onSubmit} />
  );

  return Register;
};

export default RegisterFormContainer;
