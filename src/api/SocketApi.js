import { w3cwebsocket as W3CWebSocket } from 'websocket';

const endpoint = 'ws://localhost:8080';

class SocketApi {
  init(token) {
    this.socket = new W3CWebSocket(process.env.REACT_APP_WS_ENDPOINT || endpoint);
    this.socket.addEventListener('open', () => {
      console.log(`WS connected on ${process.env.REACT_APP_WS_ENDPOINT}`);
      const event = {
        data: token,
        type: 'connection',
      };
      this.socket.send(JSON.stringify(event));
    });

    this.socket.addEventListener('error', (error) => {
      console.error(`WS error: ${error}`);
    });

    this.socket.addEventListener('close', () => {
      console.log('WS disconnected');
    });
  }

  send(message) {
    try {
      const event = {
        type: 'message',
        data: message,
      };
      this.socket.send(JSON.stringify(event));
    } catch (error) {
      console.error(error);
    }
  }

  handleMessages(handler) {
    try {
      this.socket.addEventListener('message', (event) => {
        return handler(JSON.parse(event.data));
      });
    } catch (error) {
      return error;
    }
  }
}

export default new SocketApi();
