import { productImagePlaceholder } from 'src/configs/variables';

const getProductImage = (photos) => {
  return photos ? photos[0] || productImagePlaceholder : productImagePlaceholder;
};

export default getProductImage;
