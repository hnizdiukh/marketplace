import { useDispatch, useSelector } from 'react-redux';
import routes from 'src/routes/config';
import { productsOperations } from 'src/modules/products';
import { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';

const useAddToWishlist = (user, product) => {
  const [isSaved, setSaved] = useState(false);
  const dispatch = useDispatch();

  const savedProducts = useSelector((state) => state.products.saved);

  useEffect(() => {
    if (product && savedProducts.length && savedProducts.find((p) => p._id === product._id)) {
      setSaved(true);
    }
  }, [setSaved, product, savedProducts]);

  const history = useHistory();
  const location = useLocation();

  const bind = async (isSave) => {
    if (!user) {
      history.push({ pathname: routes.LOGIN, state: { prevPath: location.pathname } });
      return;
    }
    if (isSave) {
      await dispatch(productsOperations.unsaveProduct(product));
      setSaved(false);
    } else {
      await dispatch(productsOperations.saveProduct(product));
    }
  };

  return [isSaved, bind];
};

export default useAddToWishlist;
