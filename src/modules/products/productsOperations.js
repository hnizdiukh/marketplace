import { PRODUCT_LIMIT } from 'src/configs/variables';
import * as actions from './productsActions';
import Api from '../../api';

export function fetchProducts() {
  return async function productsThunk(dispatch) {
    try {
      dispatch(actions.products.start());

      const res = await Api.Products.get({ offset: 0, limit: PRODUCT_LIMIT });

      dispatch(actions.products.success(res.data));
    } catch (err) {
      dispatch(actions.products.error({ message: err.message }));
    }
  };
}

export function fetchMoreProducts({ offset }) {
  return async function fetchMoreProductsThunk(dispatch) {
    try {
      dispatch(actions.fetchMoreProducts.start());

      const res = await Api.Products.get({ offset });
      dispatch(actions.fetchMoreProducts.success(res.data));
    } catch (err) {
      dispatch(actions.fetchMoreProducts.error({ message: err.message }));
    }
  };
}

export function addProduct(product) {
  return async function addProductThunk(dispatch) {
    try {
      dispatch(actions.addProduct.start());

      const res = await Api.Products.add(product);

      dispatch(actions.addProduct.success(res.data));
    } catch (err) {
      dispatch(actions.addProduct.error({ message: err.message }));
    }
  };
}

export function updateProduct(product) {
  return async function updateProductThunk(dispatch) {
    try {
      dispatch(actions.updateProduct.start());

      const res = await Api.Products.update(product);

      dispatch(actions.updateProduct.success(res.data));
    } catch (err) {
      dispatch(actions.updateProduct.error({ message: err.message }));
    }
  };
}

export function search(params) {
  return async function searchProductThunk(dispatch) {
    try {
      dispatch(actions.search.start());

      const res = await Api.Products.search(params);

      dispatch(actions.search.success({ res: res.data, params }));
    } catch (err) {
      dispatch(actions.search.error({ message: err.message }));
    }
  };
}

export function saveProduct(product) {
  return async function saveProductThunk(dispatch) {
    try {
      dispatch(actions.saveProduct.start());

      await Api.Products.save(product._id);

      dispatch(actions.saveProduct.success(product));
    } catch (err) {
      dispatch(actions.saveProduct.error({ message: err.message }));
    }
  };
}

export function unsaveProduct(product) {
  return async function unsaveProductThunk(dispatch) {
    try {
      dispatch(actions.unsaveProduct.start());

      await Api.Products.unsave(product._id);

      dispatch(actions.unsaveProduct.success(product));
    } catch (err) {
      dispatch(actions.unsaveProduct.error({ message: err.message }));
    }
  };
}

export function savedProducts() {
  return async function savedProductsThunk(dispatch) {
    try {
      dispatch(actions.savedProducts.start());

      const res = await Api.Products.getSaved();

      dispatch(actions.savedProducts.success(res.data));
    } catch (err) {
      dispatch(actions.savedProducts.error({ message: err.message }));
    }
  };
}

export function getProduct(productId) {
  return async function savedProductsThunk(dispatch) {
    try {
      dispatch(actions.getProduct.start());

      const res = await Api.Products.getProduct(productId);

      dispatch(actions.getProduct.success(res.data));
    } catch (err) {
      dispatch(actions.getProduct.error({ message: err.message }));
    }
  };
}

export function deleteProduct(productId) {
  return async function savedProductsThunk(dispatch) {
    try {
      dispatch(actions.getProduct.start());

      const res = await Api.Products.remove(productId);

      dispatch(actions.getProduct.success(res.data));
    } catch (err) {
      dispatch(actions.getProduct.error({ message: err.message }));
    }
  };
}

export function getUserProducts(userId) {
  return async function savedProductsThunk(dispatch) {
    try {
      dispatch(actions.getUserProducts.start());

      const res = await Api.Products.userProducts(userId);

      dispatch(actions.getUserProducts.success(res.data));
    } catch (err) {
      dispatch(actions.getUserProducts.error({ message: err.message }));
    }
  };
}
