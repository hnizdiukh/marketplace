import Api, { SocketApi } from 'src/api';
import initialization from './appActions';
import { messagesOperations } from '../messages';

export function subscribeToSockets() {
  return function subscribeToSocketsThunk(dispatch) {
    SocketApi.handleMessages((message) => {
      dispatch(messagesOperations.handleMessagesRealtime(message));
    });
  };
}

export function init() {
  return async function initThunk(dispatch) {
    try {
      dispatch(initialization.start());

      Api.init();

      dispatch(subscribeToSockets());
      dispatch(initialization.success());
    } catch (err) {
      dispatch(initialization.error({ message: err.message }));
    }
  };
}
