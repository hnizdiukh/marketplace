import createAsyncActions from '@letapp/redux-actions/lib/createAsyncActions';

const initialization = createAsyncActions('app/INITIALIZATION');
export default initialization;
