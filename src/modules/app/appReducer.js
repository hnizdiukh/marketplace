import { handleActions } from '@letapp/redux-actions';
import initialization from './appActions';

const INITIAL_STATE = {
  isLoading: false,
  isError: false,
  error: null,
};

export default handleActions(
  {
    [initialization.start]: (state) => ({
      ...state,
      isLoading: true,
      error: null,
      isError: false,
    }),
    [initialization.success]: (state) => ({ ...state, isLoading: false }),
    [initialization.error]: (state, action) => ({
      ...state,
      isLoading: false,
      error: action.payload,
      isError: true,
    }),
  },
  INITIAL_STATE
);
