import { v4 as uuid } from 'uuid';

const createMessage = ({ text, chatId, ownerId }) => ({
  _id: uuid(),
  text,
  createdAt: new Date().getTime(),
  chatId,
  ownerId,
  read: false,
});

export default createMessage;
