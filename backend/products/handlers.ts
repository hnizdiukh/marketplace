import { productSchema } from '../schemas.ts';
import { v4 } from 'https://deno.land/std/uuid/mod.ts';
import { RouterContext, helpers } from 'https://deno.land/x/oak/mod.ts';
import { Product, SearchFields } from './interfaces.ts';
import DB from '../db.ts';
import { destructureProductObject } from '../utils/destructureProductObject.ts';
import { getUserFromJWT } from '../utils/getUserFromJWT.ts';
import { validateAuthHeaders } from '../utils/validateAuthHeaders.ts';

const productCollection: any = DB.collection('products') as any;
const savedProductCollection: any = DB.collection('saved_products');
const usersCollection = DB.collection('users');

export const getProducts = async (ctx: RouterContext) => {
  try {
    const body = await ctx.request.body();
    const params = helpers.getQuery(ctx, { mergeParams: true });

    const limit: number = body.value?.limit || params.limit || 20;
    const offset: number = body.value?.offset || params.offset || 0;

    const products: Array<any> = await productCollection.aggregate([
      { $sort: { createdAt: -1 } },
      { $skip: Number(offset) },
      { $limit: Number(limit) },
    ]);

    const headers: Headers = ctx.request.headers;
    const authorization = headers.get('Authorization');
    const token = await validateAuthHeaders(ctx);
    const user = authorization && token ? await getUserFromJWT(token) : null;

    const runAsyncFunctions = async (user: any) => {
      if (!user) {
        products.map((product) => (product.saved = false));
        return;
      }

      let saved: any;
      await Promise.all(
        products.map(async (product) => {
          saved = await savedProductCollection.find({ userId: user._id, productIds: product._id });
          product.saved = !!saved.length;
        })
      );
    };

    await runAsyncFunctions(user);

    if (!products) {
      throw new Error('No products found');
    }

    ctx.response.body = products;
    ctx.response.status = 200;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const getSingleProduct = async (ctx: RouterContext) => {
  const id = ctx.params.id;
  try {
    const product = await productCollection.findOne({ _id: id });

    if (!product) {
      throw new Error(`No product with such id ${id}`);
    }

    const owner = await usersCollection.findOne({ _id: product.ownerId });
    delete owner.password;

    if (!owner) {
      throw new Error('Product owner not found');
    }

    product.owner = owner;

    ctx.response.body = product;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const updateProduct = async (ctx: RouterContext) => {
  try {
    const id = ctx.params.id;
    const body = await ctx.request.body();
    const product = await productCollection.count({ _id: id });

    if (!product) {
      throw new Error(`No product with such id ${id}`);
    }

    if (body.type !== 'json') {
      throw new Error('Invalid type');
    }

    const productToUpdate = (await productSchema.validate(body.value)) as Product;

    productToUpdate.updatedAt = Date.now();

    const destructured = destructureProductObject(productToUpdate);

    await productCollection.updateOne({ _id: id }, { $set: { ...destructured } });

    ctx.response.body = destructured;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const searchProducts = async (ctx: RouterContext) => {
  try {
    const params = helpers.getQuery(ctx, { mergeParams: true });
    const { keywords, location, priceFrom, priceTo, limit, offset }: SearchFields = params;

    let searchQuery: any = '';
    let sortByScore: any = '';

    if (keywords) {
      searchQuery = {
        $search: {
          text: {
            query: keywords?.replace('+', ' ')?.toString(),
            path: ['title', 'description'],
          },
        },
      };

      sortByScore = {
        $sort: {
          score: {
            $meta: 'textScore',
          },
        },
      };
    }

    const queryMatch: any = {};

    if (priceTo) {
      queryMatch.price = { $gte: Number(priceFrom) || 0, $lte: Number(priceTo) };
    } else {
      queryMatch.price = { $gte: Number(priceFrom) || 0 };
    }

    if (location) {
      queryMatch.location = location.replace('+', ' ');
    }

    const products: Array<any> = await productCollection.aggregate([
      searchQuery,
      {
        $match: queryMatch,
      },
      {
        $skip: offset || 0,
      },
      {
        $limit: limit || 20,
      },

      sortByScore,
    ]);

    ctx.response.body = products;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const userProducts = async (ctx: RouterContext) => {
  try {
    const id = ctx.params.id;

    if (!id) {
      throw new Error('id should be specified');
    }

    const products = await productCollection.aggregate([
      { $sort: { createdAt: -1 } },
      { $match: { ownerId: id } },
    ]);

    ctx.response.body = { list: products, count: products.length };
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const postProduct = async (ctx: RouterContext) => {
  try {
    const body = await ctx.request.body();

    if (body.type !== 'json') {
      throw new Error('Invalid type');
    }

    const product = (await productSchema.validate(body.value)) as Product;
    const token = await validateAuthHeaders(ctx);
    const user = await getUserFromJWT(token);

    product._id = v4.generate();
    product.createdAt = Date.now();
    product.ownerId = user._id;

    const destructured = destructureProductObject(product);
    await productCollection.insertOne(destructured);

    ctx.response.body = destructured;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const savedProducts = async (ctx: RouterContext) => {
  try {
    const token = await validateAuthHeaders(ctx);
    const user = await getUserFromJWT(token);
    const userSaved = await savedProductCollection.findOne({ userId: user._id });

    if (userSaved && userSaved.productIds) {
      const products = await productCollection.aggregate([
        { $sort: { createdAt: -1 } },
        { $match: { _id: { $in: userSaved.productIds } } },
      ]);

      await products.map((product: Product) => (product.saved = true));

      ctx.response.body = products;
    } else {
      ctx.response.body = [];
    }

    ctx.response.status = 200;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const saveProduct = async (ctx: RouterContext) => {
  try {
    const productId = ctx.params.id;

    if (!productId) {
      throw new Error('productId is not specified');
    }

    const token = await validateAuthHeaders(ctx);
    const user = await getUserFromJWT(token);

    const usersSaved = await savedProductCollection.findOne({ userId: user._id });
    const product = await productCollection.findOne({ _id: productId });
    let updated, created;

    if (!product) {
      throw new Error(`No product with such id ${productId}`);
    }

    if (usersSaved) {
      updated = await savedProductCollection.updateOne(
        { userId: user._id },
        { $addToSet: { productIds: productId } }
      );
    } else {
      created = await savedProductCollection.insertOne({
        userId: user._id,
        productIds: [productId],
      });
    }

    ctx.response.body =
      (updated && updated.modifiedCount) || created ? 'Saved' : 'Already was saved';
    ctx.response.status = 200;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const unsaveProduct = async (ctx: RouterContext) => {
  try {
    const productId = ctx.params.id;

    if (!productId) {
      throw new Error('productId is not specified');
    }

    const token = await validateAuthHeaders(ctx);
    const user = await getUserFromJWT(token);
    const updated = await savedProductCollection.updateOne(
      { userId: user._id },
      { $pull: { productIds: productId } }
    );

    ctx.response.body = updated && updated.modifiedCount ? 'Unsaved' : 'Not in saved';
    ctx.response.status = 200;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const removeProduct = async (ctx: RouterContext) => {
  try {
    const id = ctx.params.id;
    const product = await productCollection.findOne({ _id: id });

    const token = await validateAuthHeaders(ctx);
    const user = await getUserFromJWT(token);

    if (!product) {
      throw new Error(`No product with such id ${id}`);
    }

    if (user._id !== product.ownerId) {
      throw new Error('User is not product owner');
    }

    const removed = await productCollection.deleteOne({ _id: id });

    ctx.response.body = removed;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};
