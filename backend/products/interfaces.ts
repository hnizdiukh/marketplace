export interface Product {
  _id?: string;
  ownerId: string;
  title: string;
  description?: string;
  photos: string[];
  location: string;
  price: number;
  createdAt?: number | null;
  updatedAt?: number | null;
  saved: boolean;
}

export interface SearchFields {
  keywords?: string;
  location?: string;
  priceFrom?: number;
  priceTo?: number;
  limit?: number;
  offset?: number;
}