import { Product } from "../products/interfaces.ts";

export const destructureProductObject = (product: Product) => {
  const { _id,
    ownerId,
    title,
    description,
    photos,
    location,
    price,
    createdAt,
    updatedAt } = product;
  return {
    _id,
    ownerId,
    title,
    description,
    photos,
    location,
    price,
    createdAt,
    updatedAt
  };
} 