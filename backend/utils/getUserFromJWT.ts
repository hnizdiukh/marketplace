import { validateJwt } from 'https://deno.land/x/djwt/validate.ts';
import { config } from 'https://deno.land/x/dotenv/mod.ts';
import DB from '../db.ts';

const env = config();
const usersCollection = DB.collection('users');

export const getUserFromJWT = async (token: any) => {
  if (!token) {
    return;
  }
  const decoded: any = await validateJwt(token, env.JWT_KEY, { algorithm: 'HS256' });

  if (!decoded || !decoded.payload) {
    throw new Error('Invalid JWT');
  }

  const user = await usersCollection.findOne({ email: decoded.payload.iss });

  if (!user) {
    return;
  }

  delete user.password;

  return user;
};
