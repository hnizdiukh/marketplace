import { RouterContext } from "https://deno.land/x/oak/mod.ts";

export const validateAuthHeaders = (ctx: RouterContext) => {
  const headers: Headers = ctx.request.headers;
  const authorization = headers.get("Authorization");
  if (!authorization) {
    return;
  }
  const token = authorization.split(' ')[1];
  return token;
}
