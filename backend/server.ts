import {
  Application,
  Router,
  RouterContext,
} from "https://deno.land/x/oak/mod.ts";
import * as flags from "https://deno.land/std/flags/mod.ts";

import {
  postProduct,
  getProducts,
  getSingleProduct,
  updateProduct,
  searchProducts,
  savedProducts,
  saveProduct,
  unsaveProduct,
  userProducts,
} from "./products/handlers.ts";
import {
  login,
  register,
  getUser,
  getCurrentUser,
  updateUser,
} from "./users/handlers.ts";
import {
  createChat,
  getChats,
  sendMessage,
  getMessages,
} from "./chats/handlers.ts";
import authMiddleware from "./users/authMiddleware.ts";
import { startWebSocket } from "./chats/websocket.ts";
import { uploadImages } from "./images/handlers.ts";
import { removeProduct } from "./products/handlers.ts";

const router = new Router();
router.get("/", (ctx: RouterContext) => {
  ctx.response.body = "Marketplace API";
});
router.post("/auth/login", login);
router.post("/auth/register", register);

router.get("/account/user", authMiddleware, getCurrentUser);
router.put("/account/user", authMiddleware, updateUser);
router.get("/users/:id", authMiddleware, getUser);

router.get("/products/latest", getProducts);
router.post("/products", postProduct);
router.get("/products/saved", authMiddleware, savedProducts);
router.get("/products/search", searchProducts);
router.get("/products/:id", getSingleProduct);
router.put("/products/:id", authMiddleware, updateProduct);
router.delete("/products/:id", authMiddleware, removeProduct);
router.post("/products/:id/save", authMiddleware, saveProduct);
router.post("/products/:id/unsave", authMiddleware, unsaveProduct);

router.get("/users/:id/products", userProducts);

router.post("/products/:id/createChat", authMiddleware, createChat);
router.get("/chats", authMiddleware, getChats);
router.post("/chats/:id/messages", authMiddleware, sendMessage);
router.get("/chats/:id/messages", authMiddleware, getMessages);

router.post("/upload/images", authMiddleware, uploadImages);

const app = new Application();

startWebSocket();

app.use(router.routes());
app.use(router.allowedMethods());

app.addEventListener("error", (e: any) => {
  console.error(e.error);
});

const { args, exit } = Deno;

const DEFAULT_PORT = 8000;

const argPort = flags.parse(args).port;

const PORT = argPort ? Number(argPort) : DEFAULT_PORT;

if (isNaN(PORT)) {
  console.log("Port is not a number");
  exit(1);
}

console.log(`Listening on http://localhost:${PORT}`);
await app.listen({ port: PORT });
