import { Product } from "../products/interfaces.ts";
import { User } from "../users/interfaces.ts";

export interface Chat {
  _id: string,
  productId: string,
  ownerId: string,
  createdAt?: number,
  updatedAt?: number,
  lastMessage?: Message,
  product: Product,
  participants: [User, User]
}

export interface Message {
  id: number,
  chatId: string,
  ownerId: string,
  text: string,
  read: boolean,
  createdAt: number,
  updatedAt?: number | null
}