import DB from '../db.ts';
import { Message } from "./interfaces.ts";

const messageCollection = DB.collection('messages');
const chatCollection = DB.collection('chats');

export const insertMessage = async (msg: any) => {
  const { chatId, userId, text } = msg;
  const messagesCount = await messageCollection.count({ chatId: chatId });


  const message: Message = {
    id: messagesCount,
    chatId: chatId,
    ownerId: userId,
    text: text,
    read: false,
    createdAt: Date.now(),
    updatedAt: null
  };

  await messageCollection.insertOne(message);

  return message;
}

export const getChatPartner = async (chatId: string, userId: string) => {
  const chat = await chatCollection.findOne({ _id: chatId });
  const partner = chat.participants.filter((p: any) => p._id !== userId);
  return partner[0];
}