import { WebSocket, WebSocketServer } from 'https://deno.land/x/websocket/mod.ts';
import { getUserFromJWT } from '../utils/getUserFromJWT.ts';
import { getChatPartner } from './db.ts';

const WS_PORT = Deno.args[0] || 8080;

const wss: any = new WebSocketServer(Number(WS_PORT));
console.log(`Starting websocket on ${WS_PORT}`);

const webSockets: any = new Map();
let i: number = 0;

export const startWebSocket = () => {
  try {
    wss.on('connection', (ws: WebSocket) => {
      ws.on('message', async (message: any) => {
        const parsed = JSON.parse(message);

        switch (parsed.type) {
          case 'connection':
            if (!parsed?.data) {
              break;
            }
            const user = await getUserFromJWT(parsed?.data?.toString());

            if (!user) {
              break;
            }

            webSockets.set(user._id, ws);
            break;
          case 'message':
            const partner = await getChatPartner(parsed.data.chatId, parsed.data.ownerId);
            const partnerWS = webSockets.get(partner._id);
            if (partnerWS && partnerWS.state === 1) {
              partnerWS.send(JSON.stringify(parsed.data));
            }
            break;
          default:
            break;
        }
      });

      ws.on('close', (e: any) => {
        console.log('WS user disconected ', e);
      });
    });
  } catch (error) {
    console.error(error);
  }
};
