import { RouterContext, helpers } from 'https://deno.land/x/oak/mod.ts';
import DB from '../db.ts';
import { getUserFromJWT } from '../utils/getUserFromJWT.ts';
import { validateAuthHeaders } from '../utils/validateAuthHeaders.ts';
import { v4 } from 'https://deno.land/std/uuid/mod.ts';
import { Chat, Message } from './interfaces.ts';
import { insertMessage } from './db.ts';

const productCollection = DB.collection('products');
const chatCollection = DB.collection('chats');
const messageCollection = DB.collection('messages');
const usersCollection = DB.collection('users');

export const createChat = async (ctx: RouterContext) => {
  try {
    const productId = ctx.params.id;

    if (!productId) {
      throw new Error('id is not specified');
    }

    const product = await productCollection.findOne({ _id: productId });
    const token = await validateAuthHeaders(ctx);
    const user = await getUserFromJWT(token);

    if (!product) {
      throw new Error(`No product with such id ${productId}`);
    }

    const owner = await usersCollection.findOne({ _id: product.ownerId });
    delete owner.password;
    delete user.password;

    if (!owner) {
      throw new Error('Owner of the product is not found');
    }

    if (user._id === owner._id) {
      throw new Error('Can not create chat with yourself');
    }

    const existingChat = await chatCollection.findOne({
      product: product,
      participants: [owner, user],
    });

    if (existingChat) {
      ctx.response.body = existingChat;
      return;
    }

    const chat: Chat = {
      _id: v4.generate(),
      productId: productId,
      ownerId: product.ownerId,
      createdAt: Date.now(),
      updatedAt: Date.now(),
      product: { ...product },
      participants: [owner, user],
    };

    await chatCollection.insertOne({ ...chat });

    ctx.response.body = chat;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const getChats = async (ctx: RouterContext) => {
  try {
    const token = await validateAuthHeaders(ctx);
    const user = await getUserFromJWT(token);

    if (!user) {
      throw new Error('Not authorized');
    }

    const chats = await chatCollection.aggregate([
      { $match: { participants: { $elemMatch: { _id: user._id } } } },
      { $sort: { updatedAt: -1 } },
    ]);

    ctx.response.body = chats;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const sendMessage = async (ctx: RouterContext) => {
  try {
    const chatId = ctx.params.id;
    const body = await ctx.request.body();

    const token = await validateAuthHeaders(ctx);
    const user = await getUserFromJWT(token);

    if (!chatId) {
      throw new Error('Invalid chat id');
    }

    if (!body.value || body.type !== 'json') {
      throw new Error('Invalid request body');
    }

    const message = await insertMessage({ chatId, userId: user._id, text: body.value.text });

    await chatCollection.updateOne({ _id: chatId }, { $set: { lastMessage: message } });

    ctx.response.body = message;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const getMessages = async (ctx: RouterContext) => {
  try {
    const params = helpers.getQuery(ctx, { mergeParams: true });

    const limit: number = Number(params.limit) || 20;
    const from: number = Number(params.from) || 0;
    const chatId: string | undefined = params.id;

    if (!chatId) {
      throw new Error('Invalid id');
    }

    const messages: Array<Message> = await messageCollection.aggregate([
      { $match: { chatId: chatId, id: { $gte: from } } },
      { $sort: { createdAt: -1 } },
      { $limit: limit },
    ]);

    if (!messages) {
      throw new Error('No messages found');
    }

    ctx.response.body = messages;
    ctx.response.status = 200;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};
