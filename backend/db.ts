import { config } from 'https://deno.land/x/dotenv/mod.ts';
import { MongoClient } from 'https://deno.land/x/mongo@v0.8.0/mod.ts';

const env = config();
const client = new MongoClient();
client.connectWithUri(env.MONGODB_URI);

const DB = client.database('marketplace');

export default DB;
