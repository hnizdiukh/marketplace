import { userSchema, userUpdateSchema } from "../schemas.ts";
import { v4 } from "https://deno.land/std/uuid/mod.ts";
import { RouterContext } from "https://deno.land/x/oak/mod.ts";
import { User, UserUpdate } from "./interfaces.ts";
import DB from "../db.ts";
import { makeJwt, Jose, Payload } from "https://deno.land/x/djwt/create.ts";
import { config } from "https://deno.land/x/dotenv/mod.ts";
import { getUserFromJWT } from "../utils/getUserFromJWT.ts";
import * as bcrypt from "https://deno.land/x/bcrypt/mod.ts";
import { validateAuthHeaders } from "../utils/validateAuthHeaders.ts";

const env = config();

const usersCollection: any = DB.collection("users");
const key = env.JWT_KEY;

const header: Jose = {
  alg: "HS256",
  typ: "JWT",
};

export const login = async (ctx: RouterContext) => {
  try {
    const { value } = await ctx.request.body();
    const user = await usersCollection.findOne({ email: value.email });

    if (!user) {
      throw new Error("Wrong credentials");
    }

    const passwordMatch = await bcrypt.compareSync(
      value.password,
      user.password,
    );

    if (!passwordMatch) {
      throw new Error("Wrong password");
    }

    const payload: Payload = {
      iss: user.email,
    };

    const token = await makeJwt({ header, payload, key });

    if (!token) {
      throw new Error("JWT token is not created");
    }

    delete user.password;

    ctx.response.status = 200;
    ctx.response.body = { token, user };
  } catch (error) {
    ctx.response.status = 401;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const register = async (ctx: RouterContext) => {
  try {
    const body = await ctx.request.body();

    if (body.type !== "json") {
      throw new Error("Invalid type");
    }

    const user = (await userSchema.validate(body.value)) as User;
    user._id = v4.generate();
    user.createdAt = Date.now();
    user.password = await bcrypt.hashSync(user.password);

    const payload: Payload = {
      iss: user.email,
    };

    const token = await makeJwt({ header, payload, key });

    if (!token) {
      throw new Error("JWT token is not created");
    }

    const existedUser = await usersCollection.findOne({ email: user.email });

    if (existedUser) {
      throw new Error("User with such email is already exist");
    }

    user.location = user.location || null;
    user.avatar = user.avatar || null;
    user.phone = user.phone || null;
    user.updatedAt = user.updatedAt || null;

    await usersCollection.insertOne(user);

    delete user.password;

    ctx.response.status = 200;
    ctx.response.body = { token, user };
  } catch (error) {
    ctx.response.status = 401;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const getCurrentUser = async (ctx: RouterContext) => {
  try {
    const token = await validateAuthHeaders(ctx);
    const user = await getUserFromJWT(token);

    if (!user) {
      throw new Error("Wrong JWT token");
    }

    delete user.password;
    ctx.response.body = user;
    ctx.response.status = 200;
  } catch (error) {
    ctx.response.status = 401;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const getUser = async (ctx: RouterContext) => {
  const id = ctx.params.id;
  try {
    const user = await usersCollection.findOne({ _id: id });

    if (!user) {
      throw new Error("No user with such id");
    }

    delete user.password;
    ctx.response.body = user;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};

export const updateUser = async (ctx: RouterContext) => {
  try {
    const body = await ctx.request.body();

    if (body.type !== "json") {
      throw new Error("Invalid type");
    }

    const fieldsToUpdate =
      (await userUpdateSchema.validate(body.value)) as UserUpdate;

    const token = await validateAuthHeaders(ctx);
    const currentUser = await getUserFromJWT(token);

    if (!currentUser) {
      throw new Error("No user with such id");
    }

    const updatedUserState = await usersCollection.updateOne(
      { _id: currentUser._id },
      { $set: { ...fieldsToUpdate } },
    );

    if (!updatedUserState.modifiedCount) {
      throw new Error("Not updated");
    }

    const updatedUser = await usersCollection.findOne({ _id: currentUser._id });
    delete updatedUser.password;

    if (!updatedUser) {
      throw new Error("User is not updated");
    }

    ctx.response.body = updatedUser;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message,
    };
  }
};
