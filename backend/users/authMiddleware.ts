import { RouterContext } from 'https://deno.land/x/oak/mod.ts';
import { validateJwt } from 'https://deno.land/x/djwt/validate.ts';
import { config } from 'https://deno.land/x/dotenv/mod.ts';

const env = config();

const authMiddleware = async (ctx: RouterContext, next: any) => {
  try {
    const headers: Headers = ctx.request.headers;
    const authorization = headers.get('Authorization');
    if (!authorization) {
      ctx.response.status = 401;
      throw new Error('Not authorized');
    }
    const token = authorization.split(' ')[1];

    if (!token) {
      ctx.response.status = 401;
      throw new Error('Not authorized');
    }

    if ((await validateJwt(token, env.JWT_KEY, { algorithm: 'HS256' })).isValid) {
      await next();
    } else {
      ctx.response.status = 401;
      throw new Error('Invalid JWT token');
    }
  } catch (error) {
    ctx.response.body = { message: error.message };
  }
};

export default authMiddleware;
