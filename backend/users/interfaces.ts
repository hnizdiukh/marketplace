export interface User {
  _id?: string;
  fullName: string;
  email: string;
  password: string;
  createdAt: number;
  updatedAt?: number | null;
  location?: string | null;
  avatar?: string | null;
  phone?: string | null;
}

export interface UserUpdate {
  fullName?: string,
  avatar?: string,
  phone?: string,
  location?: string
}