import { RouterContext } from "https://deno.land/x/oak/mod.ts";

export const uploadImages = async (ctx: RouterContext) => {
  try {
    const body = await ctx.request.body();


    ctx.response.status = 200;
  } catch (error) {
    ctx.response.status = 422;
    ctx.response.body = {
      message: error.message
    }
  }
}